FROM node:9.4.0-alpine
WORKDIR /app
RUN npm install --global serve
COPY build .
EXPOSE 80
CMD ["serve", "-p", "80", "-s", "."]