import * as types from '../constants/cart';

export const addToCart = item => {
    return {
        type: types.ADD_TO_CART,
        item
    };
}

export const removeFromCart = index => {
    return {
        type: types.REMOVE_FROM_CART,
        index
    };
}
