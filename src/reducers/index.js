import menu from './menu';
import cart from './cart';

export default {
    menu,
    cart
};