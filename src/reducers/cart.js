import * as types from '../constants/cart';

const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
    const { type, item, index } = action;
    switch (type) {
        case types.ADD_TO_CART: {
            return [
                ...state,
                item
            ];
        }
        case types.REMOVE_FROM_CART: {
            let newState = [
                ...state
            ];
            newState.splice(index, 1);
            return newState;
        }
        default: {
            return state;
        }
    }
};