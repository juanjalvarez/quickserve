const INITIAL_STATE = {
    categories: [
        {
            id: 'breakfast',
            name: 'Breakfast',
            imagePath: '/assets/category-breakfast.jpg',
            subcategories: [
                {
                    id: 'pancakes',
                    name: 'Pancakes'
                },
                {
                    id: 'sandwiches',
                    name: 'Sandwiches'
                }
            ]
        },
        {
            id: 'salad',
            name: 'Salad',
            imagePath: '/assets/category-salad.jpg',
            subcategories: [
                {
                    id: 'salads',
                    name: 'Salads'
                }
            ]
        },
        {
            id: 'dessert',
            name: 'Dessert',
            imagePath: '/assets/category-dessert.jpg',
            subcategories: [
                {
                    id: 'desserts',
                    name: 'Desserts'
                }
            ]
        }
    ],
    items: [
        {
            id: 'breakfast_pancake_bannana',
            category: 'breakfast',
            subcategory: 'pancakes',
            name: 'Salted Caramel & Banana Cream Pancake',
            price: 11.99,
            description: 'Two new fluffy buttermilk pancakes with shortbread pieces cooked inside, layered with vanilla cream and topped with fresh bananas, warm salted caramel sauce and even more shortbread pieces. Served with two sunny side up eggs* and hash browns, plus your choice of two strips of bacon or two sausage links.',
            imagePath: '/assets/breakfast-pancake-bannana.jpg',
            options: [
                {
                    id: 'ingridients',
                    name: 'Ingredients',
                    type: 'checkbox',
                    values: ['Vanilla Cream', 'Bannanas', 'Salted Caramel Sauce', 'Shortbread Pieces'],
                    default: ['Vanilla Cream', 'Bannanas', 'Salted Caramel Sauce', 'Shortbread Pieces']
                },
                {
                    id: 'additional_ingredients',
                    name: 'Additional Ingredients',
                    type: 'radio',
                    values: ['Bacon', 'Sausage'],
                    default: 'Bacon'
                },
                {
                    id: 'notes',
                    name: 'Notes',
                    type: 'text',
                    default: ''
                }
            ]
        },
        {
            id: 'breakfast_pancake_dulce_de_leche',
            category: 'breakfast',
            subcategory: 'pancakes',
            name: 'Stack of Dulce de Leche Crunch Pancakes',
            price: 13.99,
            description: 'Our new cinnamon crumb topping cooked into two fluffy buttermilk pancakes and topped with whipped cream. Served with a pitcher of warm dulce de leche inspired caramel sauce for drizzling.',
            imagePath: '/assets/breakfast-pancake-dulce-de-leche.jpg',
            options: [
                {
                    id: 'ingridients',
                    name: 'Ingredients',
                    type: 'checkbox',
                    values: ['Cinnamon Crumbs', 'Whipped Cream', 'Dulce De Leche Sauce'],
                    default: ['Cinnamon Crumbs', 'Whipped Cream', 'Dulce De Leche Sauce']
                },
                {
                    id: 'notes',
                    name: 'Notes',
                    type: 'text',
                    default: ''
                }
            ]
        },
        {
            id: 'breakfast_sandwich_loaded_breakfast_sandwich',
            category: 'breakfast',
            subcategory: 'sandwiches',
            name: 'Loaded Breakfast Sandwich',
            price: 9.99,
            description: 'Two strips of bacon, shaved ham, American cheese and an egg* cooked to order on a toasted English muffin. Served with crispy hash browns cooked with diced bacon and shredded Cheddar cheese.',
            imagePath: '/assets/breakfast-sandwich-loaded-breakfast-sandwich.jpg',
            options: [
                {
                    id: 'ingridients',
                    name: 'Ingredients',
                    type: 'checkbox',
                    values: ['Bacon', 'Shaved Ham', 'American Cheese', 'Egg'],
                    default: ['Bacon', 'Shaved Ham', 'American Cheese', 'Egg']
                },
                {
                    id: 'additional_ingridients',
                    name: 'Additional Ingredients',
                    type: 'checkbox',
                    values: ['Hash Browns', 'Bacon', 'Cheddar Cheese'],
                    default: ['Hash Browns', 'Bacon', 'Cheddar Cheese']
                },
                {
                    id: 'notes',
                    name: 'Notes',
                    type: 'text',
                    default: ''
                }
            ]
        },
        {
            id: 'breakfast_sandwich_moons_over_my_hammy',
            category: 'breakfast',
            subcategory: 'sandwiches',
            name: 'Moons Over My Hammy',
            price: 11.99,
            description: 'Our classic ham and scrambled egg sandwich with Swiss and American cheeses on grilled sourdough. Served with hash browns.',
            imagePath: '/assets/breakfast-sandwich-moons-over-my-hammy.jpg',
            options: [
                {
                    id: 'ingridients',
                    name: 'Ingredients',
                    type: 'checkbox',
                    values: ['Ham', 'Scrambled Egg', 'American Cheese'],
                    default: ['Ham', 'Scrambled Egg', 'American Cheese']
                },
                {
                    id: 'notes',
                    name: 'Notes',
                    type: 'text',
                    default: ''
                }
            ]
        },
        {
            id: 'salad_cobb',
            category: 'salad',
            subcategory: 'salads',
            name: 'Chicken Cobb Salad',
            price: 8.99,
            description: 'Your choice of grilled or fried chicken, bacon, fresh avocado, grape tomatoes, Cheddar cheese, hard-boiled egg and potato sticks atop a bed of spring mix. Served with the dressing of your choice.',
            imagePath: '/assets/salad-cobb.jpg',
            options: [
                {
                    id: 'ingridients',
                    name: 'Ingredients',
                    type: 'checkbox',
                    values: ['Bacon', 'Avocado', 'Grape Tomatoes', 'Cheddar Cheese', 'Boiled Egg', 'Potato Sticks', 'Spring Mix'],
                    default: ['Bacon', 'Avocado', 'Grape Tomatoes', 'Cheddar Cheese', 'Boiled Egg', 'Potato Sticks', 'Spring Mix']
                },
                {
                    id: 'chicken',
                    name: 'Chicken',
                    type: 'radio',
                    values: ['Grilled', 'Fried', 'None'],
                    default: 'Grilled'
                },
                {
                    id: 'dressing',
                    name: 'Dressing',
                    type: 'radio',
                    values: ['Ranch', 'Cranberry', 'Caesar', 'Balsamic', 'None'],
                    default: 'Ranch'
                },
                {
                    id: 'notes',
                    name: 'Notes',
                    type: 'text',
                    default: ''
                }
            ]
        },
        {
            id: 'salad_cranberry_apple',
            category: 'salad',
            subcategory: 'salads',
            name: 'Cranberry Apple Chicken Salad',
            price: 10.99,
            description: 'Grilled seasoned chicken breast, glazed pecans, apple slices and dried cranberries atop a bed of spring mix. Served with balsamic vinaigrette.',
            imagePath: '/assets/salad-cranberry-apple.jpg',
            options: [
                {
                    id: 'ingridients',
                    name: 'Ingredients',
                    type: 'checkbox',
                    values: ['Chicken', 'Glazed Pecans', 'Apple Slices', 'Dried Cranberries', 'Spring Mix', 'Balsamic Vinaigrette'],
                    default: ['Chicken', 'Glazed Pecans', 'Apple Slices', 'Dried Cranberries', 'Spring Mix', 'Balsamic Vinaigrette']
                },
                {
                    id: 'notes',
                    name: 'Notes',
                    type: 'text',
                    default: ''
                }
            ]
        },
        {
            id: 'dessert_bannana_split',
            category: 'dessert',
            subcategory: 'desserts',
            name: 'Bannana Split',
            price: 6.99,
            description: 'Fresh banana and three generous scoops of premium ice cream with caramel, hot fudge and strawberry topping, whipped cream and chopped nuts.',
            imagePath: '/assets/dessert-bannana-split.jpg',
            options: [
                {
                    id: 'ingridients',
                    name: 'Ingredients',
                    type: 'checkbox',
                    values: ['Bannanas', 'Caramel', 'Hot Fudge', 'Strawberries', 'Whipped Cream', 'Chopped Nuts'],
                    default: ['Bannanas', 'Caramel', 'Hot Fudge', 'Strawberries', 'Whipped Cream', 'Chopped Nuts']
                },
                {
                    id: 'ice_cream',
                    name: 'Ice Cream',
                    type: 'checkbox',
                    values: ['Vanilla', 'Strawberry', 'Chocolate'],
                    default: ['Vanilla', 'Strawberry', 'Chocolate']
                },
                {
                    id: 'notes',
                    name: 'Notes',
                    type: 'text',
                    default: ''
                }
            ]
        },
        {
            id: 'dessert_new_york_cheesecake',
            category: 'dessert',
            subcategory: 'desserts',
            name: 'New York Style Cheesecake',
            price: 4.99,
            description: 'New York style cheesecake with a graham cracker crust served plain or add strawberry topping, hot fudge and whipped cream.',
            imagePath: '/assets/dessert-new-york-cheesecake.jpg',
            options: [
                {
                    id: 'ingridients',
                    name: 'Ingredients',
                    type: 'checkbox',
                    values: ['Hot Fudge', 'Whipped Cream'],
                    default: ['Hot Fudge', 'Whipped Cream']
                },
                {
                    id: 'topping',
                    name: 'Topping',
                    type: 'radio',
                    values: ['Strawberry', 'Plain'],
                    default: 'Strawberry'
                },
                {
                    id: 'notes',
                    name: 'Notes',
                    type: 'text',
                    default: ''
                }
            ]
        }
    ]
};

export default (state = INITIAL_STATE, action) => {
    return state;
};