import React from 'react';

import '../styles/xbutton.css';

export default class XButton extends React.Component {
    render() {
        const onClick = this.props.onClick;
        return(
            <div className="xbutton-wrapper" onClick={onClick}>
                <div className="xbutton-container">
                    <div className="xbutton">
                        X
                    </div>
                </div>
            </div>
        );
    }
}
