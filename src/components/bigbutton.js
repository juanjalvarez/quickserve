import React from 'react';

import '../styles/bigbutton.css';

export default class BigButton extends React.Component {
    render() {
        const { onClick, children } = this.props;
        return (
            <div className="big-button-wrapper">
                <div className="big-button" onClick={onClick}>
                    {children}
                </div>
            </div>
        );
    }
}
