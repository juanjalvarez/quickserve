import React from 'react';
import { Route, Switch, withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import FaArrowLeft from 'react-icons/lib/fa/arrow-left';
import FaShoppingCart from 'react-icons/lib/fa/shopping-cart';

import CategoryList from './categorylist';
import Category from './category';
import ItemCustomizer from './item-customizer';
import Cart from './cart';

import '../styles/layout.css';

class Layout extends React.Component {
    render() {
        const { cart } = this.props;
        const cartCount = cart.length > 0 ? <div className="cart-count">{cart.length}</div> : null;
        return (
            <div className="layout-wrapper">
                <div className="layout-header">
                    <div className="layout-header-item layout-header-back">
                        <Link to="/">
                            <span><FaArrowLeft className="layout-header-back-button" /></span>
                        </Link>
                    </div>
                    <div className="layout-header-item layout-header-logo-container">
                        <img alt="" src="/assets/logo.png" className="layout-header-logo"/>
                    </div>
                    <div className="layout-header-item layout-header-cart">
                        <Link to="/cart" className="cart-link">
                            <div className="cart-link-wrapper">
                                <FaShoppingCart className="layout-header-cart-button" />
                                {cartCount}
                            </div>
                        </Link>
                    </div>
                </div>
                <div className="layout-body">
                    <Switch>
                        <Route exact path="/" component={CategoryList} />
                        <Route exact path="/category/:categoryId" component={Category} />
                        <Route path="/category/:categoryId/item/:itemId" component={ItemCustomizer} />
                        <Route path="/cart" component={Cart} />>
                    </Switch>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cart
    };
};

export default withRouter(connect(mapStateToProps, null)(Layout));
