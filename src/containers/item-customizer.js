import React from 'react';
import { connect } from 'react-redux';

import BigButton from '../components/bigbutton';
import * as cartActions from '../actions/cart';

import '../styles/item-customizer.css';

class ItemCustomizer extends React.Component {

    constructor(props) {
        super(props);
        const { categoryId, itemId } = props.match.params;
        const menu = this.props.menu;
        //const category = menu.categories.find(e => e.id === categoryId);
        const item = itemId ? menu.items.find(e => e.id === itemId) : null;
        this.state = {
            item,
            options: []
        };
        for (const option of item.options) {
            this.state.options.push({
                id: option.id,
                type: option.type,
                value: option.default
            });
        }
        this.renderOption = this.renderOption.bind(this);
        this.onValueChange = this.onValueChange.bind(this);
        this.onCheckboxChange = this.onCheckboxChange.bind(this);
        this.addToMeal = this.addToMeal.bind(this);
    }

    onValueChange(e) {
        let options = this.state.options;
        const optionIndex = options.findIndex(o => o.id === e.target.name);
        options[optionIndex].value = e.target.value;
        this.setState({
            options
        });
    }

    onCheckboxChange(e) {
        let options = this.state.options;
        const optionIndex = options.findIndex(o => o.id === e.target.name);
        let value = options[optionIndex].value;
        debugger
        if (e.target.value === 'on') {
            value = value.splice(value.indexOf(e.target.title), 1);
        } else {
            value.push(e.target.value)
        }
        options[optionIndex].value = value;
        this.setState({
            options
        });
    }

    renderOption(option) {
        const optionState = this.state.options.find(o => o.id === option.id);
        switch (option.type) {
            case 'radio': {
                return (
                    <div className="option-radio-wrapper">
                        {
                            option.values.map((v, index) => {
                                return (
                                    <div className="option-radio-item" key={index}>
                                        <input
                                            type="radio"
                                            name={option.id}
                                            value={v}
                                            checked={v === optionState.value}
                                            className="option-radio"
                                            onChange={this.onValueChange}
                                        />
                                        {v}
                                        <br />
                                    </div>
                                );
                            })
                        }
                    </div>
                );
            }
            case 'text': {
                return (
                    <div className="option-text-wrapper">
                        <textarea
                            className="option-text"
                            placeholder="Message to the chef..."
                            name={option.id}
                            value={optionState.value}
                            onChange={this.onValueChange}
                        />
                    </div>
                );
            }
            case 'checkbox': {
                return (
                    <div className="option-checkbox-wrapper">
                        {
                            option.values.map((v, index) => {
                                return (
                                    <div className="option-checkbox-item" key={index}>
                                        <input
                                            className="option-checkbox"
                                            type="checkbox"
                                            name={option.id}
                                            title={v}
                                            defaultChecked={optionState.value.includes(v)}
                                        />
                                        {v}
                                        <br />
                                    </div>
                                );
                            })
                        }
                    </div>
                );
            }
            default: {
                return (
                    <div>
                        NO VALID OPTION TYPE '{option.type}'
                    </div>
                );
            }
        }
    }

    addToMeal() {
        let item = {
            ...this.state.item
        };
        item.options = this.state.optionState;
        this.props.addToCart(item);
        this.props.history.push('/cart')
    }

    render() {
        const { item } = this.state;
        return (
            <div className="item-customize-wrapper">
                <div className="item-customize-title">{item.name}</div>
                <div className="item-customize-options">
                    {
                        item.options.map((o, index) => {
                            return (
                                <div className="option-wrapper" key={index}>
                                    <div className="option-name">{o.name}</div>
                                    <hr />
                                    {this.renderOption(o)}
                                </div>
                            );
                        })
                    }
                </div>
                <BigButton onClick={this.addToMeal}>Add to Meal</BigButton>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        menu: state.menu
    };
};

const mapActionsToProps = dispatch => {
    return {
        addToCart: item => dispatch(cartActions.addToCart(item))
    };
};

export default connect(mapStateToProps, mapActionsToProps)(ItemCustomizer);