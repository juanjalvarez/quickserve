import React from 'react';
import { connect } from 'react-redux';

import * as cartActions from '../actions/cart';
import BigButton from '../components/bigbutton';
import XButton from '../components/xbutton';

import '../styles/cart.css';

class Cart extends React.Component {

    removeFromCart(index) {
        return () => this.props.removeFromCart(index);
    }

    render() {
        const { cart } = this.props;
        return (
            <div className="cart-wrapper">
                <div className="cart-title">Your Meal</div>
                <hr />
                <div className="cart-item-list">
                    {
                        cart.map((item, index) => {
                            return (
                                <div className="cart-item-wrapper">
                                    <div className="cart-item-info">
                                        <div className="cart-item-remove">
                                            <XButton onClick={this.removeFromCart(index)} />
                                        </div>
                                        <div className="cart-item-image-wrapper">
                                            <img alt="" src={item.imagePath} className="cart-item-image" />
                                        </div>
                                        <div className="cart-item-name">{item.name}</div>
                                    </div>
                                    <div className="cart-item-price">$ {item.price}</div>
                                </div>
                            );
                        })
                    }
                </div>
                <BigButton>
                    Send to Chef
                </BigButton>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cart
    };
};

const mapActionsToProps = dispatch => {
    return {
        removeFromCart: index => dispatch(cartActions.removeFromCart(index))
    };
};

export default connect(mapStateToProps, mapActionsToProps)(Cart);