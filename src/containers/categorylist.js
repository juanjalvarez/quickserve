import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import '../styles/categorylist.css';

class CategoryList extends React.Component {
    render() {
        const { categories } = this.props.menu;
        return (
            <div>
                {
                    categories.map((c, index) => {
                        return (
                            <Link
                                to={`/category/${c.id}`}
                                className="category-button-wrapper"
                                style={{
                                    backgroundImage: `url(${c.imagePath})`
                                }}
                                key={index}
                            >
                                <div className="category-button-name">{c.name}</div>
                            </Link>
                        );
                    })
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        menu: state.menu
    };
};

export default connect(mapStateToProps, null)(CategoryList);