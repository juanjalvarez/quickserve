import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import '../styles/category.css';

class Category extends React.Component {
    render() {
        const categoryId = this.props.match.params.categoryId;
        const menu = this.props.menu;
        const category = menu.categories.find(e => e.id === categoryId);
        const items = menu.items.filter(e => e.category === categoryId);
        return (
            <div className="category-wrapper">
                {
                    category && category.subcategories ? category.subcategories.map((sub, index1) => {
                        return (
                            <div className="subcategory-wrapper" key={index1}>
                                <div className="subcategory-name">{sub.name}</div>
                                <hr />
                                <div className="subcategory-items">
                                    {
                                        items.filter(e => e.subcategory === sub.id).map((item, index2) => {
                                            return (
                                                <div className="item-wrapper" key={index2}>
                                                    <div className="item-info-wrapper">
                                                        <div className="item-image-wrapper">
                                                            <img alt="" src={item.imagePath} className="item-image" />
                                                        </div>
                                                        <div className="item-text-wrapper">
                                                            <div className="item-name-and-price">
                                                                <span className="item-name">{item.name}</span><span className="item-price">${item.price}</span>
                                                            </div>
                                                            <div className="item-description">{item.description}</div>
                                                        </div>
                                                    </div>
                                                    <Link to={`/category/${categoryId}/item/${item.id}`} className="item-buy-link">
                                                        <div className="item-buy">
                                                            Customize
                                                        </div>
                                                    </Link>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        );
                    }) : null
                }
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        menu: state.menu,
        router: state.router
    };
};

export default connect(mapStateToProps, null)(Category);